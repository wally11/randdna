#include <iostream>
#include <string>
#include <random>

using namespace std;

string randDNA(int s, string b, int n)
{
	int min = 0;
	int max = b.size()-1;
	mt19937 eng1(s);
	uniform_int_distribution<> unorm(min,max);
	int index = 0;
	string ATCG = "";
	
	for (int i =0;i<n;i++)
	{
		index = unorm(eng1);
		ATCG += b[index];
	} 

 return ATCG;
}
